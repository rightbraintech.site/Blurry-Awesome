
# Blurry Awesome

This is a fork of floppy theme created by manilarome "https://github.com/manilarome/the-glorious-dotfiles".
 ## Screenshots

![App Screenshot](https://raw.githubusercontent.com/rightbraintech/Blurry-Awesome/main/theme/floppy-theme/blurry-awesome-screenshot.png)

![App Screenshot](https://raw.githubusercontent.com/rightbraintech/Blurry-Awesome/main/theme/floppy-theme/blurry-awesome-screenshot1.png)

![App Screenshot](https://raw.githubusercontent.com/rightbraintech/Blurry-Awesome/main/theme/floppy-theme/blurry-awesome-screenshot2.png)


## Dependencies

You can use any aur helper "yay" "aura" "paru"

```bash
 light-git alsa-utils pavucontrol pulseaudio pulseaudio-alsa flameshot rofi inter-font picom-git nautilus nitrogen acpi acpid acpi_call imagemagick xfce4-power-manager upower noto-fonts-emoji xdg-user-dirs materia-gtk-theme papirus-icon-theme

```
## Apps Recommended To Use

+ Nautilus (File-Manager)
+ Kitty (Terminal-Emulator)
+ Brave (Browser)
+ Leafpad (Text-Editor)
+ VLC (Media-Player)
+ Flameshot (Screenshot Tool)
+ Octopi (Pacman-GUI)

## Installation 

Clone the repository
```bash
git clone https://github.com/rightbraintech/Blurry-Awesome.git
```
Then cp (copy it) to .config/ in your Home folder and rename it to "awesome"  
For guys who are lazy copy && paste the given code👇
```bash
cp -r Blurry-Awesome $HOME/.config/awesome
```
## Things to change after install :
 + In ./config/awesome/module edit the exit-screen.lua there instead of my name (Aditya) replace it with your name.
 + In ./config/awesome/configuration/ edit apps.lua, here you can change the app defaults.
 + In ./config/awesome/configuration/key, here you can edit the keybindings.
 + Change the gtk and icon theme using lxappearance.
 + Change the wallpaper using nitrogen. 
 + Check all the shortcuts by pressing Win+F1.
 
 
# THANK YOU!


