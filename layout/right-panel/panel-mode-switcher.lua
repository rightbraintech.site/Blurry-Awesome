local awful = require('awful')
local wibox = require('wibox')
local gears = require('gears')
local beautiful = require('beautiful')

local dpi = require('beautiful').xresources.apply_dpi
local clickable_container = require('widget.clickable-container')

-- Variable used for switching panel modes
right_panel_mode = 'notif_mode'

local active_button    = beautiful.groups_title_bg
local inactive_button  = beautiful.transparent

local notif_text = wibox.widget
{
	text 	= 	'Notifications',
	font   	= 	'Inter Bold 11',
	align  	= 	'center',
	valign 	= 	'center',
	widget 	= 	wibox.widget.textbox
}

local notif_button = clickable_container(
	wibox.container.margin(
		notif_text, dpi(0), dpi(0), dpi(7), dpi(7)
	)
)

local wrap_notif = wibox.widget {
	notif_button,
	forced_width 	= 	dpi(140),
	bg 				= 	inactive_button,
	border_width	= 	dpi(1),
	border_color 	= 	beautiful.groups_title_bg,
	shape 			= 	function(cr, width, height) 
							gears.shape.partially_rounded_rect(
								cr, width, height, false, true, true, false, beautiful.groups_radius
							)
						end,
	widget 			= 	wibox.container.background
}









notif_button:buttons(
	gears.table.join(
		awful.button(
			{},
			1,
			nil,
			function()
				switch_rdb_pane('notif_mode')
			end
		)
	)
)

